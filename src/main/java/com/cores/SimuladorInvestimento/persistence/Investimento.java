/**
 *
 * Nome do programa:			Investimento.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	13/06/2020
 * Descrição:					Entidade contendo as informações da simulação do Investimento - Mapeamento da Tabela do B.D..
 * 
 */

package com.cores.SimuladorInvestimento.persistence;

import javax.persistence.*;
import javax.validation.constraints.Min;

import org.springframework.format.annotation.NumberFormat;

/*
 * RC: JPA - Java Persistence API - Lê objetos em Json e persiste estes objetos em um banco de dados por exemplo.
 * Esta aplicação está usando o banco de dados embedded H2, definido no application.properties (com função de armazenamento físico - parâmetro UPDATE)
 * O properties foi alterado para gravar o arquivo simulacoesInvest.mv.db na raiz do projeto.
 */

@Entity //anotação para mapeamento de uma tabela de Banco de Dados
public class Investimento {

	@Id //sempre obrigatório setar um campo pertencente a chave primária com driver JPA
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
    //@Column(unique = true)
	private int id;
	
	@NumberFormat
	@Min(1) //mínimo de 1 real
	private double valorInvestimento;
	
	@NumberFormat
	@Min(1) //mínimo de 1 mes de investimento
	private int quantidadeMeses;
	
	private double montanteAcumulado;
	
	public Investimento() {} //construtor vazio - obrigatório
	
	public Investimento(int id, double valorInvestimento, int quantidadeMeses, double montanteAcumulado) {
		this.id							= id;
		this.valorInvestimento			= valorInvestimento;
		this.quantidadeMeses			= quantidadeMeses;
		this.montanteAcumulado			= montanteAcumulado;
	}
	
	public int getId() {
		return id;
	}
	
	public double getValorInvestimento() {
		return valorInvestimento;
	}
	
	public int getQuantidadeMeses() {
		return quantidadeMeses;
	}
	
	public double getMontanteAcumulado() {
		return montanteAcumulado;
	}
	
	public void setId(int id) {
		this.id							= id;
	}
	
	public void setValorInvestimento(double valorInvestimento) {
		this.valorInvestimento			= valorInvestimento;
	}
	
	public void setQuantidadeMeses(int quantidadeMeses) {
		this.quantidadeMeses			= quantidadeMeses;
	}
	
	public void setMontanteAcumulado(double montanteAcumulado) {
		this.montanteAcumulado			= montanteAcumulado;
	}
	
}
