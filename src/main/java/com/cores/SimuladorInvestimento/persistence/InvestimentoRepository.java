/**
 *
 * Nome do programa:			InvestimentoRepository.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	13/06/2020
 * Descrição:					Interface utilizada para chamadas (consultas) ao B.D (entidade Investimentos) usando ORM (Object Relational Mapping) com driver JPA.
 * 
 */

package com.cores.SimuladorInvestimento.persistence;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<Investimento, Integer>{
	List<Investimento> findAll(); 		//select de todas as informações (instancias) desta entidade Investimento. Por exemplo, Count() devolve o numero de instancias armazenadas.
	//long count(); 

}
