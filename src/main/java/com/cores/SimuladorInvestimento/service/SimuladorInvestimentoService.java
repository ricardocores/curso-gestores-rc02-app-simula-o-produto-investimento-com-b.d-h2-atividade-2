/**
 *
 * Nome do programa:			SimuladorInvestimentoService.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	13/06/2020
 * Descrição:					Processa retorno de Simulações de Investimento (GET) e orquestra o cálculo de novas requisições (POST) - com persistência de B.D.
 * 
 */

package com.cores.SimuladorInvestimento.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cores.SimuladorInvestimento.persistence.Investimento;
import com.cores.SimuladorInvestimento.persistence.InvestimentoRepository;

@Service
public class SimuladorInvestimentoService {
	
	@Autowired
	private InvestimentoRepository investBD;
	
	@Autowired
	private SimuladorValorInvestido simulador;
	

	public List<Investimento> getAllInvestimentos(){
		
		System.out.println("Qtde de Linhas B.D.: " + investBD.count());
		return investBD.findAll(); //consulta todas as informações de investimentos do banco de dados
		
	}
	
	
	public Investimento simularInvestimento(Investimento investimento) {
		
		investimento.setMontanteAcumulado(simulador.simularInvestimento(investimento));
		investBD.save(investimento);
		
		return investimento;
	}
	
}
