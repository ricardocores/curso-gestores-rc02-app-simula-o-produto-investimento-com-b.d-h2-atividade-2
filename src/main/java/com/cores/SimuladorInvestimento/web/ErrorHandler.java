/**
 *
 * Nome do programa:			ErrorHandler.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	13/06/2020
 * Descrição:					Tratamento de Handle Exceptions na interface Web (B.D e Geral)
 * 
 */

package com.cores.SimuladorInvestimento.web;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.HashMap;

@ControllerAdvice
public class ErrorHandler {
	
    @ExceptionHandler(ConstraintViolationException.class) //Implementation of JDBCException indicating that the requested DML operation resulted in a violation of a defined integrity constraint.
    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    public HashMap<String, String> handleValidationError(ConstraintViolationException exception){
        
    	System.out.println("Entrou aqui - ConstraintViolationException");
    	HashMap<String, String> errors = new HashMap();

        for(ConstraintViolation violation : exception.getConstraintViolations()){
            errors.put(violation.getPropertyPath().toString(), violation.getMessage());
        }

        return errors;
    }
    

    @ExceptionHandler(Exception.class) //tratamento de qualquer Exception
    @ResponseStatus(code = HttpStatus.NOT_ACCEPTABLE, reason = "Deu problema - Exception Qualquer") //Este reason não está aparecendo no Postman, mas o HttpStatus sim.
    public void conflict(Exception exception) {
    	
      System.out.println("Exception - Deu qualquer problema"); //imprime na log do Spring
      System.out.println(exception.getMessage());

    }
}
