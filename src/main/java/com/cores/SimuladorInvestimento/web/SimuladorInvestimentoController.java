/**
 *
 * Nome do programa:			SimuladorInvestimentoController.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	12/06/2020
 * Descrição:					Mapeia GET e POST da API de Simulação de Investimento
 * 
 */

package com.cores.SimuladorInvestimento.web;

import org.springframework.web.bind.annotation.*;

import com.cores.SimuladorInvestimento.service.SimuladorInvestimentoService;
import com.cores.SimuladorInvestimento.web.dto.InvestimentoRequest;
import com.cores.SimuladorInvestimento.web.dto.InvestimentoResponse;
import com.cores.SimuladorInvestimento.persistence.Investimento;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

@RestController
@RequestMapping("/investimento")
public class SimuladorInvestimentoController {
	
	@Autowired
	private SimuladorInvestimentoService simuladorService;
	
	@Autowired
	private InvestimentoResponse investimentoResponse;

	
	@GetMapping
	public List<InvestimentoResponse> getAllInvestimentos(){
		
		List<Investimento> listInvestimentos = simuladorService.getAllInvestimentos();

		List<InvestimentoResponse> listInvestimentosResponse = new ArrayList<InvestimentoResponse>();
	
		//carrega Investimentos no formato da saída do payload (InvestimentoResponse)
		for(int i = 0; i < listInvestimentos.size(); i++) {
			investimentoResponse = new InvestimentoResponse();
			investimentoResponse.setValor(listInvestimentos.get(i).getValorInvestimento());
			investimentoResponse.setQuantidadeMeses(listInvestimentos.get(i).getQuantidadeMeses());
			investimentoResponse.setMontante(listInvestimentos.get(i).getMontanteAcumulado());
			listInvestimentosResponse.add(investimentoResponse);
		}
		
		return listInvestimentosResponse;
	}
	
				
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public InvestimentoResponse realizarSimulacaoInvestimento(@Valid @RequestBody InvestimentoRequest investimentoRequest) {
		
		Investimento investimento   = new Investimento(); //precisa ficar dentro do método, senão afeta demais Investimentos já gravados.
		investimentoResponse		= new InvestimentoResponse();
		
		investimento.setValorInvestimento(investimentoRequest.getValor());
		investimento.setQuantidadeMeses(investimentoRequest.getQuantidadeMeses());
		simuladorService.simularInvestimento(investimento);
		
		investimentoResponse.setValor(investimento.getValorInvestimento());
		investimentoResponse.setQuantidadeMeses(investimento.getQuantidadeMeses());
		investimentoResponse.setMontante(investimento.getMontanteAcumulado());
		
		return investimentoResponse;
	}
	
}
